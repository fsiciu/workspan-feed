import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardActionButtonsComponent } from './card-action-buttons.component';

describe('CardActionButtonsComponent', () => {
  let component: CardActionButtonsComponent;
  let fixture: ComponentFixture<CardActionButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardActionButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardActionButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { AnswerModel, PersonModel } from '../../core/models/answer.model';
import { RandomUsersService } from '../../core/services/random-users.service';
import { SubSink } from 'subsink';
import { QuestionModel } from '../../core/models/question.model';

@Component({
  selector: 'app-card-action-buttons',
  templateUrl: './card-action-buttons.component.html',
  styleUrls: ['./card-action-buttons.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CardActionButtonsComponent implements OnInit, OnDestroy {
  @Input() data: QuestionModel | AnswerModel;
  @Input() customClass: string;

  user: PersonModel;
  private subscriptions = new SubSink();

  constructor(
    private sanitizer: DomSanitizer,
    private iconRegistry: MatIconRegistry,
    private randomUserService: RandomUsersService
  ) {
    this.randomUserService.currentUser.subscribe((response: PersonModel) => this.user = response);
  }

  ngOnInit(): void {
    this.initSVGIcons();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  onUpvoteClick(): void {
    if (this.data.upVotes < 0) {
      return;
    }

    if (this.data.voters.length === 0) {
      this.data.voters.push(this.user.id);
      this.data.upVotes++;
      this.data.isVoted = true;
    } else {
      const isIncluded = this.data.voters.includes(this.user.id);

      if (!isIncluded) {
        this.data.voters.push(this.user.id);
        this.data.upVotes++;
        this.data.isVoted = true;
      }
    }

    if (!this.data.isVoted) {
      this.data.isVoted = true;
      this.data.upVotes++;
      this.data.downVotes--;
    }
  }

  onDownvoteClick(): void {
    if (this.data.downVotes < 0) {
      return;
    }

    if (this.data.voters.length === 0) {
      this.data.voters.push(this.user.id);
      this.data.downVotes++;
      this.data.isVoted = false;
    } else {
      const isIncluded = this.data.voters.includes(this.user.id);

      if (!isIncluded) {
        this.data.voters.push(this.user.id);
        this.data.downVotes++;
        this.data.isVoted = false;
      }
    }

    if (this.data.isVoted) {
      this.data.isVoted = false;
      this.data.downVotes++;
      this.data.upVotes--;
    }
  }

  private initSVGIcons(): void {
    this.iconRegistry.addSvgIcon(
      'like',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/img/like.svg'));

    this.iconRegistry.addSvgIcon(
      'dislike',
      this.sanitizer.bypassSecurityTrustResourceUrl('assets/img/dislike.svg'));
  }
}

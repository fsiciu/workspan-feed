import { Component, OnInit, AfterViewInit, ViewEncapsulation, Input, Type, Injector, ChangeDetectorRef } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, NgControl } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputComponent,
      multi: true
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class InputComponent implements OnInit, ControlValueAccessor, AfterViewInit {
  @Input() placeholder = '';
  @Input() label = '';
  @Input() customClass: string;
  @Input() inputId: string;

  model: any;
  control: FormControl;

  constructor(
    private injector: Injector,
    private changeDetector: ChangeDetectorRef
  ) {
  }

  ngOnInit(): void {
    if (!this.inputId) {
      do {
        this.inputId = `id_${Math.random()
          .toString(36)
          .substring(2)}`;
      } while (document.getElementById(this.inputId));
    }
  }

  // The form control is only set after initialization
  ngAfterViewInit(): void {
    // tslint:disable-next-line:no-null-keyword
    const ngControl = this.injector.get<NgControl>(<Type<NgControl>>NgControl, null);
    if (ngControl) {
      this.control = <FormControl>ngControl.control;
      this.changeDetector.detectChanges();
    }
  }

  // Begin ControlValueAccessor methods.
  // tslint:disable-next-line
  onChange = (change: any): void => {
    return change;
  };

  onTouched = (): void => {
  };

  // Form model content changed.
  writeValue(content: any): void {
    this.model = content;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  // End ControlValueAccessor methods.
}

import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { QuestionModel } from '../../core/models/question.model';
import { AnswersService } from '../../core/services/answers.service';
import { QuestionsService } from '../../core/services/questions.service';
import { ENVIRONMENT } from '../../../environments/environment';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CardComponent implements OnInit {
  @Input() question: QuestionModel;

  answerForm: FormGroup;

  constructor(
    private questionsService: QuestionsService,
    private answersService: AnswersService,
    private router: Router,
    private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.initForm();
  }

  onAnswerAdd(form: FormGroup): void {
    if (form.valid) {
      this.questionsService.addNewAnswerToQuestion(form.value.newAnswer, this.question);
      this.answersService.sortAnswersByDate(this.question.answers);
      this.updateLocalStorageQuestions(this.question);
      form.reset();
    }
  }

  goToQuestionDetails(question: QuestionModel): void {
    this.questionsService.setSelectedQuestion(question);
    this.router.navigate([`/question-details/`, `${question.id}`]);
  }

  private initForm(): void {
    this.answerForm = this.fb.group({
      newAnswer: ['']
    });
  }

  private updateLocalStorageQuestions(question: QuestionModel): void {
    const questions = JSON.parse(localStorage.getItem(ENVIRONMENT.STORAGE_KEYS.QA)).map((item: QuestionModel) => {
      if (item.id === question.id) {
        item.answers = [...question.answers];
      }
      return item;
    });

    localStorage.setItem(ENVIRONMENT.STORAGE_KEYS.QA, JSON.stringify(questions));
  }
}

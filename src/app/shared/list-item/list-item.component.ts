import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { AnswerModel } from '../../core/models/answer.model';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListItemComponent implements OnInit {
  @Input() answer: AnswerModel;

  constructor() {
  }

  ngOnInit(): void {
  }

}

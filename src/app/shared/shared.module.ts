import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCardModule, MatDividerModule, MatIconModule, MatInputModule, MatListModule } from '@angular/material';

import { CardComponent } from './card/card.component';
import { ListItemComponent } from './list-item/list-item.component';
import { InputComponent } from './input/input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardActionButtonsComponent } from './card-action-buttons/card-action-buttons.component';

@NgModule({
  declarations: [
    CardComponent,
    ListItemComponent,
    InputComponent,
    CardActionButtonsComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CardComponent,
    ListItemComponent,
    InputComponent
  ]
})
export class SharedModule {
}

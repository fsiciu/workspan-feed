import { Component, OnDestroy, OnInit } from '@angular/core';
import { QuestionModel } from '../../core/models/question.model';
import { QuestionsService } from '../../core/services/questions.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.scss']
})
export class QuestionDetailsComponent implements OnInit, OnDestroy {
  selectedQuestion: QuestionModel;

  private subscriptions = new SubSink();

  constructor(private questionService: QuestionsService) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.questionService.currentSelectedQuestion.subscribe((response: QuestionModel) => {
      this.selectedQuestion = response;
    }));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QUESTION_DETAILS_ROUTING } from './question-details-routing';
import { QuestionDetailsComponent } from './question-details.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    QuestionDetailsComponent
  ],
  imports: [
    CommonModule,
    QUESTION_DETAILS_ROUTING,
    SharedModule
  ]
})
export class QuestionDetailsModule {
}

import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionDetailsComponent } from './question-details.component';

const routes: Routes = [
  {
    path: '',
    component: QuestionDetailsComponent
  }
];

export const QUESTION_DETAILS_ROUTING: ModuleWithProviders = RouterModule.forChild(routes);

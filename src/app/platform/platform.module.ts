import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PLATFORM_ROUTING } from './platform-routing.module';
import { PlatformComponent } from './platform.component';
import { HeaderComponent } from './components/header/header.component';
import { MatButtonModule, MatIconModule, MatListModule, MatMenuModule } from '@angular/material';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    HeaderComponent,
    PlatformComponent
  ],
  imports: [
    CommonModule,
    PLATFORM_ROUTING,
    SharedModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatListModule
  ]
})
export class PlatformModule {
}

import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlatformComponent } from './platform.component';

const routes: Routes = [
  {
    path: '',
    component: PlatformComponent,
    children: [
      {
        path: '',
        redirectTo: 'questions',
        pathMatch: 'full'
      },
      {
        path: 'questions',
        loadChildren: './questions/questions.module#QuestionsModule'
      },
      {
        path: 'question-details/:id',
        loadChildren: './question-details/question-details.module#QuestionDetailsModule'
      }
    ]
  }
];

export const PLATFORM_ROUTING: ModuleWithProviders = RouterModule.forChild(routes);

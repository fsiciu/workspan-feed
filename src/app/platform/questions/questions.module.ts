import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';

import { QUESTIONS_ROUTING } from './questions-routing.module';
import { QuestionsComponent } from './questions.component';

@NgModule({
  declarations: [
    QuestionsComponent
  ],
  imports: [
    CommonModule,
    QUESTIONS_ROUTING,
    SharedModule
  ]
})
export class QuestionsModule {
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { SubSink } from 'subsink';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { QuestionModel } from '../../core/models/question.model';
import { QuestionsService } from '../../core/services/questions.service';
import { RandomUsersService } from '../../core/services/random-users.service';
import { AnswersService } from '../../core/services/answers.service';
import { ENVIRONMENT } from '../../../environments/environment';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit, OnDestroy {
  private subscriptions = new SubSink();
  questions: Array<QuestionModel> = [];

  constructor(
    private questionsService: QuestionsService,
    private answersService: AnswersService,
    private randomUserService: RandomUsersService
  ) {
  }

  ngOnInit(): void {
    this.initQA();
    this.randomUserService.generateRandomUser();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  initQA(): void {
    if (localStorage.getItem(ENVIRONMENT.STORAGE_KEYS.QA)) {
      this.questions = JSON.parse(localStorage.getItem(ENVIRONMENT.STORAGE_KEYS.QA));
    } else {
      this.getQuestionsAndAnswers();
    }
  }

  private getQuestionsAndAnswers(): void {
    this.subscriptions.add(forkJoin(this.questionsService.getQuestions(), this.answersService.getAnswers())
      .subscribe(([questionsResponse, answersResponse]: any) => {
        this.questions = this.questionsService.mapAnswersToQuestion(questionsResponse, answersResponse);
        this.questions.map((item: QuestionModel) => this.answersService.sortAnswersByDate(item.answers));
        localStorage.setItem(ENVIRONMENT.STORAGE_KEYS.QA, JSON.stringify(this.questions));
      }));
  }
}

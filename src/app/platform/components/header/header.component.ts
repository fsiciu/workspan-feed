import { Component, OnDestroy, OnInit } from '@angular/core';
import { PersonModel } from '../../../core/models/answer.model';
import { RandomUsersService } from '../../../core/services/random-users.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  user: PersonModel;
  private subscriptions = new SubSink();

  constructor(
    private randomUsersService: RandomUsersService
  ) {
  }

  ngOnInit(): void {
    this.subscriptions.add(this.randomUsersService.currentUser.subscribe((response: PersonModel) => this.user = response));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}

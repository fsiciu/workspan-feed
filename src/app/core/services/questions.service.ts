import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiFeedQuestionsModel, ApiQuestionModel, Question, QuestionAdapter, QuestionModel } from '../models/question.model';
import { ENVIRONMENT } from '../../../environments/environment';
import { Answer, AnswerModel, PersonModel } from '../models/answer.model';
import { RandomUsersService } from './random-users.service';
import { SubSink } from 'subsink';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService implements OnDestroy {

  private selectedQuestion = new BehaviorSubject<QuestionModel>(new Question());
  private user: PersonModel;
  private subscriptions = new SubSink();

  currentSelectedQuestion = this.selectedQuestion.asObservable();

  constructor(
    private http: HttpClient,
    private questionAdapter: QuestionAdapter,
    private randomUserService: RandomUsersService
  ) {
    this.subscriptions.add(this.randomUserService.currentUser.subscribe((response: PersonModel) => this.user = response));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  setSelectedQuestion(value: QuestionModel): void {
    this.selectedQuestion.next(value);
  }

  getQuestions(): Observable<Array<QuestionModel>> {
    return this.http.get(`${ENVIRONMENT.API.QUESTIONS}`)
      .pipe(map((data: ApiFeedQuestionsModel) => data.feed_questions.map((question: ApiQuestionModel) => this.questionAdapter.adapt(question))));
  }

  mapAnswersToQuestion(questions: Array<QuestionModel>, answers: Array<AnswerModel>): Array<QuestionModel> {
    return questions.filter((question: QuestionModel) => answers.filter((answer: AnswerModel) => {
      if (question.id === answer.questionId) {
        return question.answers.push(answer);
      }
    }));
  }

  addNewAnswerToQuestion(newAnswer: string, question: QuestionModel): void {
    question.answers.push(new Answer({
      questionId: question.id,
      answer: newAnswer,
      createdBy: this.user,
      createdAt: new Date(),
      id: Math.random().toString()
    }));
  }
}

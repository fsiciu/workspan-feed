import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnswerAdapter, AnswerModel, ApiAnswerModel, ApiFeedAnswersModel } from '../models/answer.model';
import { ENVIRONMENT } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AnswersService {

  constructor(
    private http: HttpClient,
    private answerAdapter: AnswerAdapter
  ) {
  }

  getAnswers(): Observable<Array<AnswerModel>> {
    return this.http.get(`${ENVIRONMENT.API.ANSWERS}`)
      .pipe(map((data: ApiFeedAnswersModel) => data.feed_answers.map((answer: ApiAnswerModel) => this.answerAdapter.adapt(answer))));
  }

  sortAnswersByDate(answersArr: Array<AnswerModel>): void {
    answersArr.sort((firstAnswer: AnswerModel, secondAnswer: AnswerModel) =>
      new Date(secondAnswer.createdAt).getTime() - new Date(firstAnswer.createdAt).getTime());
  }
}

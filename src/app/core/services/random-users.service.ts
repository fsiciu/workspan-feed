import { Injectable } from '@angular/core';
import { Person, PersonModel } from '../models/answer.model';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RandomUsersService {
  private randomUser = new BehaviorSubject<PersonModel>(new Person());
  currentUser = this.randomUser.asObservable();

  users: Array<PersonModel> = [
    {
      id: '1',
      name: 'John',
      surname: 'Travolta',
      avatar: 'https://api.adorable.io/avatars/201'
    },
    {
      id: '2',
      name: 'Christina',
      surname: 'Bruen',
      avatar: 'https://api.adorable.io/avatars/285'
    },
    {
      id: '3',
      name: 'Deron',
      surname: 'Krajcik',
      avatar: 'https://api.adorable.io/avatars/3'
    }
  ];

  generateRandomUser(): void {
    this.randomUser.next(this.users[Math.floor(Math.random() * this.users.length)]);
  }
}

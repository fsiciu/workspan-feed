import { Injectable } from '@angular/core';
import { Adapter } from '../adapter';
import { AnswerModel } from './answer.model';

export interface ApiFeedQuestionsModel {
  feed_questions: Array<ApiQuestionModel>;
}

export interface ApiQuestionModel {
  Id: string;
  Text: string;
  upvotes?: string;
  downvotes?: string;
}

export interface QuestionModel {
  id: string;
  text: string;
  upVotes: number;
  downVotes: number;
  answers?: Array<AnswerModel>;
  voters?: Array<string>;
  isVoted?: boolean;
  createdAt?: string;
}

export class Question implements QuestionModel {
  id = '';
  text = '';
  upVotes = 0;
  downVotes = 0;
  answers ? = [];
  voters ? = [];
  isVoted ? = false;

  constructor(dto?: QuestionModel) {
    if (!dto) {
      return;
    }
    this.id = dto.id;
    this.text = dto.text;
    this.upVotes = dto.upVotes || this.upVotes;
    this.downVotes = dto.downVotes || this.downVotes;
  }
}

@Injectable({
  providedIn: 'root'
})

export class QuestionAdapter implements Adapter<Question> {
  adapt(item: ApiQuestionModel): QuestionModel {
    return new Question({
      id: item.Id,
      text: item.Text,
      upVotes: Number(item.upvotes),
      downVotes: Number(item.downvotes)
    });
  }
}

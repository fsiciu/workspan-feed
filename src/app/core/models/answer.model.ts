import { Injectable } from '@angular/core';
import { Adapter } from '../adapter';

export interface ApiFeedAnswersModel {
  feed_answers: Array<ApiAnswerModel>;
}

export interface ApiAnswerModel {
  Id: string;
  ['Question-Id']: string;
  Answer: string;
  upvotes: string;
  downvotes: string;
  created_at: string;
  created_by: string | ApiPersonModel;
}

export interface ApiPersonModel {
  Id: string;
  Name: string;
  Surname: string;
  Avatar: string;
}

export interface AnswerModel {
  id: string;
  questionId: string;
  answer: string;
  upVotes?: number;
  downVotes?: number;
  createdAt: Date;
  createdBy: string | PersonModel;
  voters?: Array<string>;
  isVoted?: boolean;
}

export interface PersonModel {
  id: string;
  name: string;
  surname: string;
  avatar: string;
}

export class Person implements PersonModel {
  id = '';
  name = '';
  surname = '';
  avatar = '';

  constructor(dto?: PersonModel) {
    if (!dto) {
      return;
    }
    this.id = dto.id;
    this.name = dto.name;
    this.surname = dto.surname;
    this.avatar = dto.avatar;
  }
}

export class Answer implements AnswerModel {
  id = '';
  questionId = '';
  answer = '';
  upVotes ? = 0;
  downVotes ? = 0;
  createdAt = new Date();
  createdBy: string | PersonModel = '';
  voters ? = [];
  isVoted ? = false;

  constructor(dto?: AnswerModel) {
    if (!dto) {
      return;
    }
    this.id = dto.id;
    this.questionId = dto.questionId;
    this.answer = dto.answer;
    this.upVotes = dto.upVotes || this.upVotes;
    this.downVotes = dto.downVotes || this.downVotes;
    this.createdAt = dto.createdAt;
    this.createdBy = dto.createdBy;
  }
}

@Injectable({
  providedIn: 'root'
})

export class AnswerAdapter implements Adapter<Answer> {
  adapt(item: ApiAnswerModel): AnswerModel {
    return new Answer({
      id: item.Id,
      questionId: item['Question-Id'],
      answer: item.Answer,
      upVotes: Number(item.upvotes),
      downVotes: Number(item.downvotes),
      createdAt: new Date(item.created_at),
      createdBy: this.mapCreatedBy(item.created_by)
    });
  }

  private mapCreatedBy(createdBy: string | ApiPersonModel): string | PersonModel {
    if (typeof createdBy === 'string') {
      return createdBy;
    }

    if (typeof createdBy === 'object') {
      return new Person({ id: createdBy.Id, name: createdBy.Name, surname: createdBy.Surname, avatar: createdBy.Avatar });
    }

    // tslint:disable-next-line:no-null-keyword
    return null;
  }
}

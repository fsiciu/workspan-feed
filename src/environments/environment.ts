// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `ENVIRONMENT.ts` with `ENVIRONMENT.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const ENVIRONMENT = {
  production: false,
  API: {
    QUESTIONS: 'https://api.myjson.com/bins/dck5b',
    ANSWERS: 'https://api.myjson.com/bins/hildr'
  },
  STORAGE_KEYS: {
    QA: 'questions_answers'
  }
};

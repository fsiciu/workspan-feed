export const ENVIRONMENT = {
  production: true,
  API: {
    QUESTIONS: 'https://api.myjson.com/bins/dck5b',
    ANSWERS: 'https://api.myjson.com/bins/hildr'
  },
  STORAGE_KEYS: {
    QA: 'questions_answers'
  }
};
